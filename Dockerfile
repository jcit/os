FROM debian:buster

ENV CODE /src
ENV OSDEV_TOOLCHAIN /toolchain

RUN DEBIAN_FRONTEND=noninteractive apt-get --quiet --yes update \
    && DEBIAN_FRONTEND=noninteractive apt-get --quiet --yes install \
        binutils \
        gcc \
        g++ \
        nasm \
        make \
        wget \
        libmpc-dev \
        grub \
        grub-pc-bin \
        xorriso \
        xz-utils \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists

COPY tools/build.sh ${OSDEV_TOOLCHAIN}/build.sh
RUN ${OSDEV_TOOLCHAIN}/build.sh

ENV PATH="${OSDEV_TOOLCHAIN}/bin:${PATH}"
WORKDIR ${CODE}
ENTRYPOINT ["/bin/bash"]
