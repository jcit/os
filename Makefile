ARCH	:= i386

SRCDIRS	:= boot kernel
OBJDIR	:= obj

SOURCES	:= $(shell find . -name "*.asm" -o -name "*.c" -o -name "*.cpp")
OBJECTS	:= $(foreach OBJECT, $(patsubst %.asm, %.o, $(patsubst %.c, %.o, $(patsubst %.cpp, %.o, $(SOURCES)))), $(OBJDIR)/$(OBJECT))
TARGET	:= os.bin

LIBC_SOURCES	:= $(shell find libc -name "*.asm" -o -name "*.c" -o -name "*.cpp")
LIBC_OBJECTS	:= $(foreach OBJECT, $(patsubst %.asm, %.o, $(patsubst %.c, %.o, $(patsubst %.cpp, %.o, $(LIBC_SOURCES)))), $(OBJDIR)/$(OBJECT))

GLOB_CFLAGS := -I./ -Ilibc/include -Iarch/$(ARCH) -D$(ARCH)

AR		:= $(ARCH)-elf-ar
AS		:= nasm -g
ASFLAGS	:= -felf32
CC		:= $(ARCH)-elf-gcc
CFLAGS	:= -std=gnu99 -ffreestanding -O2 -Wall -Wextra -g $(GLOB_CFLAGS)
LD		:= $(ARCH)-elf-gcc
LDFLAGS	:= -ffreestanding -O2 -nostdlib -lgcc
CX		:= $(ARCH)-elf-g++
CXFLAGS := -ffreestanding -O2 -Wall -Wextra -fno-exceptions -fno-rtti -g $(GLOB_CFLAGS)

.PHONY: all clean

all: os

debug: CFLAGS += -DDEBUG -g
debug: CXFLAGS += -DDEBUG -g
debug: ASFLAGS += -g
debug: os

os: os.bin

clean:
	rm -rf $(OBJDIR)
	rm -rf lib
	rm $(TARGET)

lib/libc.a : $(LIBC_OBJECTS)
	mkdir -p lib
	$(AR) rcs $@ $(LIBC_OBJECTS)

$(TARGET) : $(OBJECTS) lib/libc.a
	mkdir -p $(@D)
	$(LD) $(LDFLAGS) -o $@ -T linker.ld $+ lib/libc.a

$(OBJDIR)/%.o : %.asm
	mkdir -p $(@D)
	$(AS) $(ASFLAGS) $< -o $@

$(OBJDIR)/%.o : %.c
	mkdir -p $(@D)
	$(CC) -c $< -o $@ $(CFLAGS)

$(OBJDIR)/%.o : %.cpp
	mkdir -p $(@D)
	$(CX) -c $< -o $@ $(CXFLAGS)
