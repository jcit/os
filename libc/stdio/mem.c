#include <stdio.h>

void *memset(void *ptr, int val, size_t size) {
    unsigned char *p = ptr;

    while (size > 0) {
        *p = val;
        p++;
        size--;
    }

    return(ptr);
}