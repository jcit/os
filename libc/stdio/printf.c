#include <stdio.h>
#include <stdarg.h>

// kernel space for now, lacking sys calls
#include <kernel/display/tty.h>

void putchar(char c) {
    tty_putchar(c);
}

void puts(char* c) {
    tty_writeline(c);
}

void puts_no_new(char* c) {
    tty_writestring(c);
}

void newline() {
    tty_newline();
}

char *convert(unsigned int num, int base) {
	static char alphabet[]= "0123456789ABCDEF";
	static char buffer[50];
	char *ptr; 

	ptr = &buffer[49]; 
	*ptr = '\0'; 
	
	do {
		*--ptr = alphabet[num%base];
		num /= base;
	} while(num != 0);
	
	return(ptr); 
}

void printf(char *format, ...) {
    char *traverse;
    unsigned int i;
    int d;
    char *s;

    va_list arg;
    va_start(arg, format);

    for (traverse = format; *traverse != '\0'; traverse++) {
        while (*traverse != '%') {
            if (*traverse == '\0' || *traverse == '\n') {
                break;
            }

            putchar(*traverse);
            traverse++;
        }

        if (*traverse == '\0') {
            break;
        }

        if (*traverse == '\n') {
            newline();
            continue;
        }

        traverse++;

        if (*traverse == '#') {
            putchar('0');
            putchar('x');
            traverse++;
        }

        switch(*traverse) {
            // char
            case 'c':
                i = va_arg(arg, int);
                putchar(i);
                break;

            // decimal
            case 'd':
                d = va_arg(arg, int);
                if (d < 0) {
                    i = -d;
                    putchar('-');
                } else {
                    i = d;
                }

                puts_no_new(convert(i, 10));
                break;

            // octal
            case 'o':
                i = va_arg(arg, unsigned int);
                puts_no_new(convert(i, 8));
                break;

            // string
            case 's':
                s = va_arg(arg, char *);
                puts_no_new(s);
                break;

            // hex
            case 'x':
                i = va_arg(arg, unsigned int);
                puts_no_new(convert(i, 16));
                break;
        }
    }

    va_end(arg);
}