#ifndef _LIBC_STDIO_H
#define _LIBC_STDIO_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif
void *memset(void *ptr, int val, size_t size);
void putchar(char c);
void puts(char* c);
void printf(char *format, ...);
#ifdef __cplusplus
}
#endif

#ifdef __cplusplus
void *operator new(size_t size);
void *operator new[](size_t size);
void operator delete(void *p);
void operator delete[](void *p);
#endif

#endif