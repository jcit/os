#ifndef _LIBC_STRING_H
#define _LIBC_STRING_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif

size_t strlen(const char* str);
char* strcpy(char* dest, char* src);

#ifdef __cplusplus
}
#endif

#endif