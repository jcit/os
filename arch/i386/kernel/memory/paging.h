#ifndef _KERNEL_I386_MEMORY_PAGING_H
#define _KERNEL_I386_MEMORY_PAGING_H

#include <stdint.h>
#include <kernel/cpu/multiboot.h>

#define PAGE_SIZE 4096

#ifdef __cplusplus
extern "C" {
#endif

void paging_init(multiboot_info_t *mbootPtr);

#ifdef __cplusplus
}
#endif

#endif