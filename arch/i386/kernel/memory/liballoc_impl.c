#include "liballoc_impl.h"
#include "frames.h"

#ifdef __cplusplus
extern "C" {
#endif

int liballoc_lock() {
    return 1;
}

int liballoc_unlock() {
    return 1;
}

void* liballoc_alloc(int pages) {
    void* ptr;

    for (int i = 0; i < pages; i++) {
        uint32_t addr = alloc_frame();

        if (i == 0) {
            ptr = addr;
        }
    }

    return ptr;
}

int liballoc_free(void* ptr, int pages) {
    return 1;
}

#ifdef __cplusplus
}
#endif