#ifndef MEMORY_LIBALLOC_IMPL_H
#define MEMORY_LIBALLOC_IMPL_H

#ifdef __cplusplus
extern "C" {
#endif

int   liballoc_lock();
int   liballoc_unlock();
void* liballoc_alloc(int);
int   liballoc_free(void*,int);

#ifdef __cplusplus
}
#endif

#endif // MEMORY_LIBALLOC_IMPL_H
