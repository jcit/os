gdt_data:
; null descriptor
    dd 0
    dd 0

; gdt code
    dw 0FFFFh       ; limit low
    dw 0            ; base low
    db 0            ; base middle
    db 10011010b    ; access
    db 11001111b    ; granularity
    db 0            ; base high

; gdt data
    dw 0FFFFh       ; limit low
    dw 0            ; base low
    db 0            ; base middle
    db 10010010b    ; access
    db 11001111b    ; granularity
    db 0            ; base high
gdt_end:

toc:
    dw gdt_end - gdt_data - 1   ; limit
    dd gdt_data                 ; base

global gdt_load
gdt_load:
    cli             ; clear interrupts
    pusha           ; save registers
    lgdt [toc]      ; load gdt
    sti             ; enable interrupts
    popa            ; load registers
    ret