#include "paging.h"
#include "frames.h"

uint32_t *page_directory;
uint32_t *page_table;

extern void load_page_dir(uint32_t *);
extern void enable_paging();

void paging_init(multiboot_info_t *mbootPtr) {
    // Setup page frame allocator
    frame_alloc_set_mmap(mbootPtr);

    page_directory = (uint32_t*)alloc_frame();

    int i;
    for (i = 0; i < 1024; i++) {
        page_directory[i] = 0x00000002; // not present
    }

    page_table = (uint32_t*)alloc_frame();

    for (i = 0; i < 1024; i++) {
        page_table[i] = (i * 0x1000) | 3;
    }

    page_directory[0] = ((unsigned int) page_table) | 3;

    load_page_dir(page_directory);
    enable_paging();
}