#ifndef _KERNEL_I386_MEMORY_FRAMES_H
#define _KERNEL_I386_MEMORY_FRAMES_H

#include <stdint.h>
#include <kernel/cpu/multiboot.h>

uint32_t mboot_reserved_start;
uint32_t mboot_reserved_end;
uint32_t next_free;

multiboot_info_t *mboot;

#define MMAP_GET_NUM 0
#define MMAP_GET_ADDR 1

void frame_alloc_set_mmap(multiboot_info_t *mbootPtr);
uint32_t alloc_frame();

#endif