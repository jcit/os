#include <kernel/drivers/keyboard/keyboard.h>
#include <sys/io.h>
#include <stdio.h>
#include <kernel/interrupts/idt.h>

static void keyboard_intr_callback(registers_t *regs);

static void keyboard_intr_callback(registers_t *regs) {
    uint8_t scancode = inb(0x60);
    keyboard_callback(scancode);
}

void keyboard_init() {
    register_interrupt_handler(IRQ1, keyboard_intr_callback);
    printf("Keyboard interrupt registered\n");
}