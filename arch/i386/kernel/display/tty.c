#include <kernel/display/tty.h>
#include <sys/io.h>
#include <string.h>
#include <stdint.h>
#include <stdbool.h>

/* Hardware text mode color constants. */
enum vga_color {
	VGA_COLOR_BLACK = 0,
	VGA_COLOR_BLUE = 1,
	VGA_COLOR_GREEN = 2,
	VGA_COLOR_CYAN = 3,
	VGA_COLOR_RED = 4,
	VGA_COLOR_MAGENTA = 5,
	VGA_COLOR_BROWN = 6,
	VGA_COLOR_LIGHT_GREY = 7,
	VGA_COLOR_DARK_GREY = 8,
	VGA_COLOR_LIGHT_BLUE = 9,
	VGA_COLOR_LIGHT_GREEN = 10,
	VGA_COLOR_LIGHT_CYAN = 11,
	VGA_COLOR_LIGHT_RED = 12,
	VGA_COLOR_LIGHT_MAGENTA = 13,
	VGA_COLOR_LIGHT_BROWN = 14,
	VGA_COLOR_WHITE = 15,
};

uint8_t vga_color_from_ansi(uint8_t ansi) {
	if (
		(ansi > 39 && ansi < 90) ||
		(ansi > 89 && ansi < 108)
	) {
		ansi -= 10;
	}

	switch (ansi)
	{
		case 30:
			return VGA_COLOR_BLACK;
		case 31:
			return VGA_COLOR_RED;
		case 32:
			return VGA_COLOR_GREEN;
		case 33:
			return VGA_COLOR_BROWN;
		case 34:
			return VGA_COLOR_BLUE;
		case 35:
			return VGA_COLOR_MAGENTA;
		case 36:
			return VGA_COLOR_CYAN;

		case 90:
			return VGA_COLOR_LIGHT_GREY;
		case 91:
			return VGA_COLOR_LIGHT_RED;
		case 92:
			return VGA_COLOR_LIGHT_GREEN;
		case 93:
			return VGA_COLOR_LIGHT_BROWN;
		case 94:
			return VGA_COLOR_LIGHT_BLUE;
		case 95:
			return VGA_COLOR_LIGHT_MAGENTA;
		case 96:
			return VGA_COLOR_LIGHT_CYAN;

		default:
			return VGA_COLOR_WHITE;
	}
}

static inline uint8_t vga_entry_color(enum vga_color fg, enum vga_color bg)
{
	return fg | bg << 4;
}
 
static inline uint16_t vga_entry(unsigned char uc, uint8_t color)
{
	return (uint16_t) uc | (uint16_t) color << 8;
}

static const size_t VGA_WIDTH = 80;
static const size_t VGA_HEIGHT = 25;
 
size_t tty_row;
size_t tty_column;
uint8_t tty_color;
uint16_t* tty_buffer;

void tty_init(void)
{
	tty_row = 0;
	tty_column = 0;
	tty_color = vga_entry_color(VGA_COLOR_LIGHT_GREY, VGA_COLOR_BLACK);
	tty_buffer = (uint16_t*) 0xB8000;
	for (size_t y = 0; y < VGA_HEIGHT; y++) {
		for (size_t x = 0; x < VGA_WIDTH; x++) {
			const size_t index = y * VGA_WIDTH + x;
			tty_buffer[index] = vga_entry(' ', tty_color);
		}
	}

	tty_cursor_enable();
}

void tty_setcolor(uint8_t color)
{
	tty_color = color;
}

void tty_putentryat(char c, uint8_t color, size_t x, size_t y)
{
	const size_t index = y * VGA_WIDTH + x;
	tty_buffer[index] = vga_entry(c, color);
	tty_update_cursor();
}

void tty_putchar(char c)
{
	if (c == '\n') {
		tty_newline();
		return;
	}

	tty_putentryat(c, tty_color, tty_column, tty_row);
	if (++tty_column == VGA_WIDTH) {
		tty_newline();
	}
}

void tty_write(const char* data, size_t size)
{
	// ANSI escape sequence state
	bool inEsqSeq = false;

	for (size_t i = 0; i < size; i++) {
		// Check for start of sequence
		if (data[i] == '\033' && data[i+1] == '[') {
			i += 2;

			int fgAnsi = ((data[i] - '0') * 10) + (data[i+1] - '0');
			uint8_t fgVga = vga_color_from_ansi(fgAnsi);

			int bgAnsi = ((data[i+3] - '0') * 10) + (data[i+4] - '0');
			uint8_t bgVga = vga_color_from_ansi(bgAnsi);

			i += 6;

			tty_setcolor(vga_entry_color(fgVga, bgVga));
		}

		// If not in sequence, display normal char
		if (!inEsqSeq) tty_putchar(data[i]);
	}
}

void tty_writestring(const char* data)
{
	tty_write(data, strlen(data));
}

void tty_writeline(const char* data) {
	tty_writestring(data);
	tty_newline();
}

void tty_newline() {
	tty_column = 0;
	if (++tty_row == VGA_HEIGHT) {
		tty_row = VGA_HEIGHT - 1;

		// move all text up a row
		for (int i = 0; i < (VGA_WIDTH * VGA_HEIGHT) - VGA_WIDTH; i++) {
			tty_buffer[i] = tty_buffer[i + VGA_WIDTH];
		}

		// blank bottom row
		for (int i = 0; i < VGA_WIDTH; i++) {
		 	size_t index = (VGA_HEIGHT - 1) * VGA_WIDTH + i;
			tty_buffer[index] = vga_entry(' ', tty_color);
		}
	}

	tty_cursor_position(tty_column, tty_row);
}

void tty_backspace() {
	if (tty_column == 0) {
		tty_row--;
		tty_column = VGA_WIDTH - 1;
	} else {
		tty_column--;
	}

	tty_putentryat(' ', tty_color, tty_column, tty_row);
	tty_cursor_position(tty_column, tty_row);
}

void tty_cursor_enable() {
	outb(0x3D4, 0x0A);
	outb(0x3D5, 0xC0);
}

void tty_cursor_disable() {
	outb(0x3D4, 0x0A);
	outb(0x3D5, 0x20);
}

void tty_cursor_position(size_t x, size_t y) {
	uint16_t pos = y * VGA_WIDTH + x;

	outb(0x3D4, 0x0F);
	outb(0x3D5, (uint8_t) (pos & 0xFF));
	outb(0x3D4, 0x0E);
	outb(0x3D5, (uint8_t) ((pos >> 8) & 0xFF));
}

void tty_update_cursor() {
	size_t cx = tty_column;
	size_t cy = tty_row;

	if (++cx == VGA_WIDTH) {
		cx = 0;
		cy++;
	}

	tty_cursor_position(cx, cy);
}

