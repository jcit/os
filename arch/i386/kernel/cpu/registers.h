#ifndef _KERNEL_I386_CPU_REGISTERS_H
#define _KERNEL_I386_CPU_REGISTERS_H

#include <stdint.h>

typedef struct
{
    uint32_t ds;
    uint32_t edi, esi, ebp, useless, ebx, edx, ecx, eax;
    uint32_t int_no, err_code;
    uint32_t eip, cs, eflags, esp, ss;
} registers_t;

#endif