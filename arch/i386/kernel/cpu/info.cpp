#include <kernel/cpu/info.h>
#include <kernel/memory/malloc.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>

#define VENDOR_AMD   "AuthenticAMD"
#define VENDOR_INTEL "GenuineIntel"

static inline void cpuid(uint32_t id, uint32_t *eax, uint32_t *ebx, uint32_t *ecx, uint32_t *edx) {
    asm volatile("cpuid"
        : "=a" (*eax), "=b" (*ebx), "=c" (*ecx), "=d" (*edx)
        : "0" (id));
}

CPUInfo::CPUInfo() {
    
}

char* CPUInfo::get_vendor() {
    uint32_t eax;
    char vendor[13];
    cpuid(0, &eax, (uint32_t*)(vendor + 0), (uint32_t*)(vendor + 8), (uint32_t*)(vendor + 4));

    if (eax > 0) {
        char* vendorPtr = (char*)malloc(sizeof(char) * strlen(vendor));
        strcpy(vendorPtr, vendor);
        return vendorPtr;
    } else {
        return "";
    }
}

void CPUInfo::print_info() {
    char* vendor = this->get_vendor();

    printf("Vendor: %s\n", vendor);

    free(vendor);
}