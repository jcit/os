; Multiboot header consts
MBALIGN  equ  1 << 0
MEMINFO  equ  1 << 1
FLAGS    equ  MBALIGN | MEMINFO
MAGIC    equ  0x1BADB002
CHECKSUM equ -(MAGIC + FLAGS)

; Multiboot declared
section .multiboot
align 4
    dd MAGIC
    dd FLAGS
    dd CHECKSUM
 
; Stack
section .bss
align 16
stack_bottom:
    resb 16384 ; 16 KiB
stack_top:

; Bootloader main
section .text
global _start:function (_start.end - _start)
_start:
        ; At this point, the multiboot bootloader has put us into
        ; 32-bit protected mode without paging or interrupts. The
        ; kernel will turn these on later in the boot sequence

        ; Set ESP to top of stack
        mov esp, stack_top

        ; Push multiboot headers
        push eax
        push ebx

        ; Run kernel
        extern kernel_main
        call kernel_main

        ; Halt
.hang:  hlt
        jmp .hang
.end: