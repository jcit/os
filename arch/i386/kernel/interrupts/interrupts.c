#include <kernel/interrupts/interrupts.h>
#include "idt.h"
#include "timer.h"
#include <stdio.h>

void interrupts_init() {
    printf("Setting interrupts\n");
    idt_init();
    // timer_init(50);
}