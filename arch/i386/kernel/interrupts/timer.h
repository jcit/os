#ifndef _KERNEL_I386_TIMER_H
#define _KERNEL_I386_TIMER_H

#include <stdint.h>

void timer_init(uint32_t freq);

#endif