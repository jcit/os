#include <kernel/display/tty.h>
#include <kernel/drivers/keyboard/keyboard.h>
#include <kernel/interrupts/interrupts.h>
#include <kernel/cpu/multiboot.h>
#include <kernel/cpu/info.h>
#include <kernel/memory/paging.h>
#include <kernel/memory/malloc.h>
#include <stdio.h>

extern "C" {
extern void gdt_load();

void kernel_main(struct multiboot_info *mboot, uint32_t magic) {
    // Setup terminal first for debugging
    tty_init();

    // Multiboot
    // if (magic == MULTIBOOT_BOOTLOADER_MAGIC) {
    //     printf("Booted from %s\n", mboot->boot_loader_name);

    //     uint32_t mem_total = mboot->mem_lower + mboot->mem_upper;
    //     printf("Detected %dMB of memory\n", mem_total / 1024 + 1);

    //     multiboot_memory_map_t* mmap = mboot->mmap_addr;

    //     while (mmap < mboot->mmap_addr + mboot->mmap_length) {
    //         printf("MMAP addr %#x", mmap->addr);
    //         printf(" len %#x", mmap->len);
    //         printf(" size %#x", mmap->size);
    //         printf(" type %#x\n", mmap->type);

    //         mmap = (multiboot_memory_map_t*) ((unsigned int)mmap + mmap->size + sizeof(mmap->size));
    //     }
    // }

    // Init core components
    gdt_load();
    interrupts_init();
    keyboard_init();

    paging_init(mboot);

    CPUInfo* info = new CPUInfo();
    info->print_info();

    printf("\nWelcome\n");

    tty_putchar('\n');
}
}