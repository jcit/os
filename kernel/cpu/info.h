#ifndef _KERNEL_CPU_INFO_H
#define _KERNEL_CPU_INFO_H

class CPUInfo {
    public:
        CPUInfo();
        char* get_vendor();
        void print_info();
};

#endif