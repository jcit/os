#include "panic.h"
#include <kernel/display/tty.h>

void panic() {
    // tty_init();
    tty_writeline("kernel panic");
    asm volatile("cli"); // disable interrupts
    asm volatile("hlt"); // stop executing
}