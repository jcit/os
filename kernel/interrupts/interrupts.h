#ifndef _KERNEL_INTERRUPTS_H
#define _KERNEL_INTERRUPTS_H

#ifdef __cplusplus
extern "C" {
#endif

void interrupts_init();

#ifdef __cplusplus
}
#endif

#endif