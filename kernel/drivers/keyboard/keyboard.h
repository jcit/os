#ifndef _KERNEL_KEYBOARD_H
#define _KERNEL_KEYBOARD_H

#include <stdint.h>

#ifdef __cplusplus
extern "C" {
#endif

void keyboard_init();
void keyboard_callback(uint8_t scancode);
void keyboard_backspace();
void keyboard_newline();

#ifdef __cplusplus
}
#endif

#endif