#include "keyboard.h"
#include <kernel/display/tty.h>
#include <stdio.h>
#include <stdbool.h>

#define BACKSPACE 0x0E
#define ENTER 0x1C

#define L_SHIFT 0x2A
#define R_SHIFT 0x36

static bool key_buffer[256];

const char *sc_name[] = { "ERROR", "Esc", "1", "2", "3", "4", "5", "6",
    "7", "8", "9", "0", "-", "=", "Backspace", "Tab", "Q", "W", "E",
        "R", "T", "Y", "U", "I", "O", "P", "[", "]", "Enter", "Lctrl",
        "A", "S", "D", "F", "G", "H", "J", "K", "L", ";", "'", "`",
        "LShift", "\\", "Z", "X", "C", "V", "B", "N", "M", ",", ".",
        "/", "RShift", "Keypad *", "LAlt", "Spacebar"};
const char sc_ascii_upper[] = { '?', '?', '!', '@', '£', '$', '%', '^',
    '&', '*', '(', ')', '_', '+', '?', '?', 'Q', 'W', 'E', 'R', 'T', 'Y',
        'U', 'I', 'O', 'P', '{', '}', '?', '?', 'A', 'S', 'D', 'F', 'G',
        'H', 'J', 'K', 'L', ':', '"', '~', '?', '\\', 'Z', 'X', 'C', 'V',
        'B', 'N', 'M', '<', '>', '?', '?', '?', '?', ' '};
const char sc_ascii_lower[] = { '?', '?', '1', '2', '3', '4', '5', '6',
    '7', '8', '9', '0', '-', '=', '?', '?', 'q', 'w', 'e', 'r', 't', 'y',
        'u', 'i', 'o', 'p', '[', ']', '?', '?', 'a', 's', 'd', 'f', 'g',
        'h', 'j', 'k', 'l', ';', '\'', '`', '?', '|', 'z', 'x', 'c', 'v',
        'b', 'n', 'm', ',', '.', '/', '?', '?', '?', ' '};


void keyboard_callback(uint8_t scancode) {
    switch (scancode)
    {
        case BACKSPACE:
            keyboard_backspace();
            break;

        case ENTER:
            keyboard_newline();
            break;

        case L_SHIFT:
            key_buffer[L_SHIFT] = true;
            break;

        case R_SHIFT:
            key_buffer[R_SHIFT] = true;
            break;

        default:
            if (scancode > 0x80) {
                // key up
                size_t key = scancode - 0x80;
                key_buffer[key] = false;
            } else {
                key_buffer[scancode] = true;

                char character;

                if (key_buffer[L_SHIFT] || key_buffer[R_SHIFT]) {
                    character = sc_ascii_upper[(int)scancode];
                } else {
                    character = sc_ascii_lower[(int)scancode];
                }

                tty_putchar(character);
            }
            break;
    }
}

void keyboard_backspace() {
    tty_backspace();
}

void keyboard_newline() {
    tty_newline();
}