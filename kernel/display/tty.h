#ifndef _KERNEL_TTY_H
#define _KERNEL_TTY_H

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif
void tty_init(void);

void tty_writestring(const char* data);
void tty_writeline(const char* data);
void tty_putchar(char c);
void tty_newline();
void tty_backspace();

void tty_cursor_enable();
void tty_cursor_disable();
void tty_cursor_position(size_t x, size_t y);
void tty_update_cursor();
#ifdef __cplusplus
}
#endif

#endif