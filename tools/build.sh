#!/bin/sh

CROSSROOT="${OSDEV_TOOLCHAIN:-$HOME/cross}"
TOOLSPREFIX="$CROSSROOT/tools"
SRC="$CROSSROOT/src"
TARGET=i386-elf

PATH="$TOOLSPREFIX/bin:$PATH"

echo Installing OS toolchain to $CROSSROOT
mkdir -p $TOOLSPREFIX
mkdir -p $SRC
cd $SRC

# Binutils
echo Downloading Binutils
wget https://ftp.gnu.org/gnu/binutils/binutils-2.32.tar.xz

echo Extracting Binutils
tar xf binutils-2.32.tar.xz

echo Building Binutils
mkdir build-binutils
cd build-binutils
../binutils-2.32/configure --target=$TARGET --prefix="$TOOLSPREFIX" --with-sysroot --disable-nls --disable-werror
make
make install

# GCC
cd $SRC

echo Downloading GCC
wget https://ftp.gnu.org/gnu/gcc/gcc-9.1.0/gcc-9.1.0.tar.xz

echo Extracting GCC
tar xf gcc-9.1.0.tar.xz

echo Building GCC
mkdir build-gcc
cd build-gcc
../gcc-9.1.0/configure --target=$TARGET --prefix="$TOOLSPREFIX" --disable-nls --enable-languages=c,c++ --without-headers
make all-gcc
make all-target-libgcc
make install-gcc
make install-target-libgcc

echo Make sure you add $TOOLSPREFIX/bin to your PATH for this to work
